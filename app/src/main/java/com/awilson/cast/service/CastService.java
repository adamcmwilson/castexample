package com.awilson.cast.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.awilson.cast.CastSession;
import com.awilson.cast.model.MediaSessionDetails;

import hugo.weaving.DebugLog;

public class CastService extends Service {

public static final String KEY_PLAYLIST = "KEY_PLAYLIST";
public static final String KEY_TITLE    = "KEY_TITLE";
public static final String KEY_EPISODE  = "KEY_EPISODE";

private CastSession castSession;

public CastService() { }

@DebugLog @Override public IBinder onBind(Intent intent) {
	throw new UnsupportedOperationException("Not yet implemented");
}

@DebugLog @Override public void onCreate() {
	super.onCreate();
	castSession = CastSession.get(getApplicationContext());
}

@DebugLog @Override public int onStartCommand(Intent intent, int flags, int startId) {

	if (intent != null && intent.getExtras() != null) {

		final String PLAYLIST = intent.getExtras().getString(KEY_PLAYLIST);
		final String TITLE = intent.getExtras().getString(KEY_TITLE);
		final String EPISODE = intent.getExtras().getString(KEY_EPISODE);

		castSession.setMediaSessionDetails(
				new MediaSessionDetails(PLAYLIST, 0, false, TITLE, EPISODE));
	}

	return START_STICKY;
}
}
