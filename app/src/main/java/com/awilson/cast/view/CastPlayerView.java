package com.awilson.cast.view;

import android.content.Context;
import android.support.v7.app.MediaRouteButton;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.awilson.cast.CastPlayer;
import com.awilson.cast.CastSession;
import com.awilson.cast.R;
import com.awilson.cast.eventbus.EventBus;
import com.awilson.cast.model.MediaSessionDetails;
import com.awilson.cast.util.DateTimeFormatter;
import com.google.android.gms.cast.MediaStatus;
import com.squareup.otto.Subscribe;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import hugo.weaving.DebugLog;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class CastPlayerView extends FrameLayout {

final int SKIP_FORWARD_DURATION  = 30000;
final int SKIP_BACKWARD_DURATION = -20000;

@InjectView(R.id.text_title)           TextView         text_title;
@InjectView(R.id.text_player_name)     TextView         text_player_name;
@InjectView(R.id.loading_layout)       ViewGroup        loading_layout;
@InjectView(R.id.lblStatus)            TextView         text_status;
@InjectView(R.id.scrubber_layout)      ViewGroup        scrubberLayout;
@InjectView(R.id.seekBar)              SeekBar          seekBar;
@InjectView(R.id.seekBarTouchDelegate) SeekBar          seekBarTouchDelegate;
@InjectView(R.id.lblTimeElapsed)       TextView         lblTimeElapsed;
@InjectView(R.id.lblTimeRemaining)     TextView         lblTimeRemaining;
@InjectView(R.id.btnSkipBack)          ImageButton      btnSkipBack;
@InjectView(R.id.btnSkipForward)       ImageButton      btnSkipForward;
@InjectView(R.id.btnTogglePlayback)    ImageButton      btnTogglePlayback;
@InjectView(R.id.mediaRouteButton)     MediaRouteButton mediaRouteButton;

private CastPlayer        castPlayer;
private DateTimeFormatter dateTimeFormatter;
private boolean seekBarTouchTracking = false;
private int     streamDuration;
private boolean prepared;

public CastPlayerView(Context context) { this(context, null);}

public CastPlayerView(Context context, AttributeSet attrs) { this(context, attrs, 0);}

public CastPlayerView(Context context, AttributeSet attrs, int defStyleAttr) {
	super(context, attrs, defStyleAttr);
	dateTimeFormatter = new DateTimeFormatter();
}

@DebugLog @OnClick(R.id.btnSkipForward) void skipForward() {
	castPlayer.seekBy(SKIP_FORWARD_DURATION);
}

@DebugLog @OnClick(R.id.btnSkipBack) void skipBackward() {
	castPlayer.seekBy(SKIP_BACKWARD_DURATION);
}

@DebugLog @OnClick(R.id.btnTogglePlayback) void togglePlayback() {
	castPlayer.togglePlayback();
}

@Override protected void onAttachedToWindow() {
	super.onAttachedToWindow();
	init();
}

@DebugLog @Override protected void onDetachedFromWindow() {
	EventBus.get().unregister(this);
	super.onDetachedFromWindow();
}

@DebugLog private void uiEnabled(final boolean enabled) {
	seekBar.setEnabled(prepared && enabled);
	seekBarTouchDelegate.setEnabled(prepared && enabled);
	btnSkipBack.setEnabled(prepared && enabled);
	btnSkipForward.setEnabled(prepared && enabled);
	btnTogglePlayback.setEnabled(prepared && enabled);
}

@DebugLog private void init() {
	ButterKnife.inject(this);
	uiEnabled(false);
	EventBus.get().register(this);

	observeMediaSessionDetails();

	CastSession castSession = CastSession.get(getContext());
	castPlayer = CastPlayer.get(getContext());

	mediaRouteButton.setRouteSelector(castSession.getSelector());
	text_player_name.setText(castSession.getCastDevice().getFriendlyName());

	seekBarTouchDelegate.setOnTouchListener(new SeekBarDelegateTouchListener());
	seekBar.setOnSeekBarChangeListener(new SeekBarListener());
}

@DebugLog @SuppressWarnings("unused")
@Subscribe public void onRemoteMediaPlayerStatusUpdate(final MediaStatus mediaStatus) {
	switch (mediaStatus.getPlayerState()) {
		case MediaStatus.PLAYER_STATE_PLAYING:
			observePlaybackPosition();
			uiEnabled(true);
			updateTogglePlaybackButtonState(true);
			text_status.setText("Playing");
			loading_layout.setVisibility(View.INVISIBLE);
			break;
		case MediaStatus.PLAYER_STATE_PAUSED:
			uiEnabled(true);
			updateTogglePlaybackButtonState(false);
			text_status.setText("Paused");
			loading_layout.setVisibility(View.INVISIBLE);
			break;
		case MediaStatus.PLAYER_STATE_BUFFERING:
			uiEnabled(false);
			text_status.setText("Buffering...");
			loading_layout.setVisibility(View.VISIBLE);
			break;
		case MediaStatus.PLAYER_STATE_IDLE:
			uiEnabled(false);
			text_status.setText("Idle...");
			loading_layout.setVisibility(View.VISIBLE);
			break;
		case MediaStatus.PLAYER_STATE_UNKNOWN:
			uiEnabled(false);
			loading_layout.setVisibility(View.INVISIBLE);
		default:
			break;
	}

	switch (mediaStatus.getIdleReason()) {
		case MediaStatus.IDLE_REASON_CANCELED:
			text_status.setText("Cancelled");
			break;
		case MediaStatus.IDLE_REASON_ERROR:
			text_status.setText("Error");
			break;
		case MediaStatus.IDLE_REASON_FINISHED:
			text_status.setText("Finished");
			break;
		case MediaStatus.IDLE_REASON_INTERRUPTED:
			text_status.setText("Interrupted");
			break;
		case MediaStatus.IDLE_REASON_NONE:
		default:
			text_status.setText("");
			break;
	}

	try {
		final int position = (int) mediaStatus.getStreamPosition();
		streamDuration = (int) mediaStatus.getMediaInfo().getStreamDuration();
		CastSession.get(getContext()).onStreamDurationChanged(streamDuration);

		if (position > 0 && streamDuration > 0) {
			updatePlaybackPosition(position);
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
}

protected void updatePlaybackPosition(final int position) {
	final int timeRemaining = -(streamDuration - position); // Time remaining is negative

	lblTimeElapsed.setText(dateTimeFormatter.getTimeString(position));
	lblTimeRemaining.setText(dateTimeFormatter.getTimeString(timeRemaining));

	seekBar.setMax(streamDuration);
	seekBar.setProgress(position);

	if (streamDuration > 0) scrubberLayout.setVisibility(View.VISIBLE);
	else scrubberLayout.setVisibility(View.INVISIBLE);
}

@DebugLog private void updateTogglePlaybackButtonState(final boolean isPlaying) {
	btnTogglePlayback.setImageResource(getTogglePlaybackResourceID(isPlaying));
}

@DebugLog private int getTogglePlaybackResourceID(final boolean isPlaying) {
	return isPlaying
	       ? R.drawable.button_video_player_pause
	       : R.drawable.button_video_player_play;
}


private void observePlaybackPosition() {
	CastPlayer.get(getContext())
	          .observePlaybackPosition()
	          .subscribeOn(Schedulers.newThread())
	          .observeOn(AndroidSchedulers.mainThread())
	          .subscribe(new Action1<Long>() {
		          @Override public void call(Long duration) {
			          updatePlaybackPosition(duration.intValue());
		          }
	          });
}

private void observeMediaSessionDetails() {
	CastSession.get(getContext())
	           .mediaSessionDetails()
	           .subscribe(new MediaSessionDetailsObserver());
}

private class SeekBarDelegateTouchListener implements OnTouchListener {
	@DebugLog @Override public boolean onTouch(View v, MotionEvent event) {
		seekBar.onTouchEvent(event);
		return false;
	}
}

private class SeekBarListener implements SeekBar.OnSeekBarChangeListener {

	@Override public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		if (!fromUser || !seekBarTouchTracking) return;
		updatePlaybackPosition(progress);
	}

	@Override public void onStartTrackingTouch(SeekBar seekBar) {
		seekBarTouchTracking = true;
	}

	@Override public void onStopTrackingTouch(SeekBar seekBar) {
		seekBarTouchTracking = false;
		castPlayer.seekTo(seekBar.getProgress());
	}
}

private class MediaSessionDetailsObserver implements Observer<MediaSessionDetails> {
	@Override public void onNext(MediaSessionDetails mediaSessionDetails) {
		uiEnabled(true);
		text_title.setText(mediaSessionDetails.getTitle());
		prepared = true;
	}

	@Override public void onError(Throwable e) { }

	@Override public void onCompleted() { }
}

}
