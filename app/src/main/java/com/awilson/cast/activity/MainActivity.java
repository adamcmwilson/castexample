package com.awilson.cast.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.MediaRouteButton;

import com.awilson.cast.CastSession;
import com.awilson.cast.R;
import com.awilson.cast.eventbus.EventBus;
import com.awilson.cast.model.CastSessionEvent;
import com.awilson.cast.service.CastService;
import com.awilson.cast.status.CastStatusMessage;
import com.squareup.otto.Subscribe;

import butterknife.ButterKnife;
import butterknife.InjectView;
import hugo.weaving.DebugLog;

public class MainActivity extends ActionBarActivity {

private final String KEY_CURRENT_VIEW = "KEY_CURRENT_VIEW";
private       String PLAYLIST         = "http://192.168.1.183/stream/pl.m3u8?5ZCiRqdc7FRxf7EOVHuRFw";
private       String TITLE            = "Chef Abroad";
private       String EPISODE          = "Tokyo Food Adventure";

private int currentLayout;

@InjectView(R.id.mediaRouteButton) MediaRouteButton mediaRouteButton;

@DebugLog @Override protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	setContentView(
			savedInstanceState == null
			? R.layout.activity_main
			: savedInstanceState.getInt(KEY_CURRENT_VIEW, R.layout.activity_main));

	ButterKnife.inject(this);

	// Allow for params to be passed via intent bundle.
	// These will override the hard-coded values being used for test.
	// Note: an intent filter will need to be setup in the manifest.
	final Intent launchIntent = getIntent();
	if (launchIntent != null) {
		final Bundle bundle = launchIntent.getExtras();
		if (bundle != null) {
			PLAYLIST = bundle.getString(CastService.KEY_PLAYLIST, PLAYLIST);
			TITLE = bundle.getString(CastService.KEY_TITLE, TITLE);
			EPISODE = bundle.getString(CastService.KEY_EPISODE, EPISODE);
		}
	}

	Intent castService = new Intent(getApplicationContext(), CastService.class);
	castService.putExtra(CastService.KEY_PLAYLIST, PLAYLIST);
	castService.putExtra(CastService.KEY_TITLE, TITLE);
	castService.putExtra(CastService.KEY_EPISODE, EPISODE);
	startService(castService);
}

@Override protected void onResume() {
	super.onResume();
	EventBus.get().register(this);
	triggerCastDeviceScan();
}

@Override protected void onPause() {
	EventBus.get().unregister(this);
	super.onPause();
}

@DebugLog @Override protected void onStop() {
	CastSession.get(getApplicationContext()).stop();
	super.onStop();
}

@Override public void setContentView(int layoutResID) {
	super.setContentView(layoutResID);
	this.currentLayout = layoutResID;
}

@Override protected void onSaveInstanceState(Bundle outState) {
	outState.putInt(KEY_CURRENT_VIEW, this.currentLayout);
	super.onSaveInstanceState(outState);
}

@DebugLog @SuppressWarnings("unused")
@Subscribe public void onCastSessionEvent(final CastSessionEvent event) {
	if (event.isStart()) setContentView(R.layout.view_cast_player);
	else if (event.isStop()) setLocalPlayback();
}

private void setLocalPlayback() {
	setContentView(R.layout.activity_main);
	triggerCastDeviceScan();
}

@DebugLog @SuppressWarnings("unused")
@Subscribe public void onCastStatusMessage(CastStatusMessage message) {
	if (message.isLoadFailed()) setLocalPlayback();
}

private void triggerCastDeviceScan() {
	mediaRouteButton.setRouteSelector(
			CastSession.get(getApplicationContext()).getSelector());
	CastSession.get(getApplicationContext()).scan();
}

}
