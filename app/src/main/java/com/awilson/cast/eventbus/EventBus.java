package com.awilson.cast.eventbus;

import android.os.Handler;
import android.os.Looper;

import com.squareup.otto.Bus;

public class EventBus extends Bus {

private final  Handler  handler;
private static EventBus instance;

private EventBus() {
	handler = new Handler(Looper.getMainLooper());
}

public static EventBus get() {
	if (instance == null)
		instance = new EventBus();
	return instance;
}

// used to ensure events are handled on the main thread
@Override public void post(final Object event) {
	if (Looper.myLooper() == Looper.getMainLooper())
		super.post(event);

	handler.post(new Runnable() {
		@Override public void run() {
			EventBus.super.post(event);
		}
	});
}
}
