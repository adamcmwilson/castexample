package com.awilson.cast;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.util.Log;

import com.awilson.cast.eventbus.EventBus;
import com.awilson.cast.model.CastSessionEvent;
import com.awilson.cast.model.MediaSessionDetails;
import com.awilson.cast.status.CastStatusMessage;
import com.awilson.cast.util.CastPreferences;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.CastMediaControlIntent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;

import hugo.weaving.DebugLog;
import rx.Observable;
import rx.subjects.BehaviorSubject;

public class CastSession extends Cast.Listener
		implements GoogleApiClient.OnConnectionFailedListener,
		           GoogleApiClient.ConnectionCallbacks {

private final String TAG    = "CastSession";
private final String APP_ID = "XXXXXXXXX";
private final String APP_NS = "urn:x-cast:com.google.cast.media";

private static CastSession          castSession;
private        Context              context;
private        CastDevice           castDevice;
private        CastPreferences      castPreferences;
private        GoogleApiClient      apiClient;
private        MediaRouter          mediaRouter;
private        MediaRouterCallbacks mediaRouterCallbacks;
private        MediaRouteSelector   mediaRouteSelector;
private        MediaSessionDetails  mediaSessionDetails;
private        boolean              applicationStarted;

private BehaviorSubject<MediaSessionDetails> mediaSessionDetailsSubject;

public static CastSession get(Context context) {
	if (castSession == null) castSession = new CastSession(context);
	return castSession;
}

private CastSession(Context context) {
	this.context = context;
	mediaRouter = MediaRouter.getInstance(context);
	mediaRouterCallbacks = new MediaRouterCallbacks();
	mediaRouteSelector = new MediaRouteSelector.Builder().addControlCategory(
			CastMediaControlIntent.categoryForCast(APP_ID)).build();

	castPreferences = new CastPreferences(context);
}

public void setMediaSessionDetails(final MediaSessionDetails details) {
	this.mediaSessionDetails = details;
	mediaSessionDetailsSubject = BehaviorSubject.create(details);
	mediaSessionDetailsSubject.buffer(1);
}

@DebugLog public MediaRouteSelector getSelector() {
	return mediaRouteSelector;
}

@DebugLog public CastDevice getCastDevice() {
	return castDevice;
}

@DebugLog public GoogleApiClient getApiClient() {
	return apiClient;
}

@DebugLog public Observable<MediaSessionDetails> mediaSessionDetails() {
	return mediaSessionDetailsSubject.asObservable();
}

@DebugLog public void scan() {
	mediaRouter.removeCallback(mediaRouterCallbacks);
	mediaRouter.addCallback(mediaRouteSelector,
	                        mediaRouterCallbacks,
	                        MediaRouter.CALLBACK_FLAG_PERFORM_ACTIVE_SCAN);
}

@DebugLog public void stop() {
	mediaRouter.removeCallback(mediaRouterCallbacks);
}

@DebugLog private void setCastDevice(CastDevice device) {
	castDevice = device;

	if (castDevice != null) {
		try {
			stopApplication();
			disconnectApiClient();
			connectApiClient();
		} catch (IllegalStateException e) {
			Log.w(TAG, "Exception while connecting API client", e);
			disconnectApiClient();
		}

		notifyListeners_onStartSession();

	} else {
		if (apiClient != null) {
			disconnectApiClient();
		}

		mediaRouter.selectRoute(mediaRouter.getDefaultRoute());
	}
}

@DebugLog private void connectApiClient() {
	Cast.CastOptions apiOptions = Cast.CastOptions
			.builder(castDevice, this).build();

	apiClient = new GoogleApiClient.Builder(context)
			.addApi(Cast.API, apiOptions)
			.addConnectionCallbacks(this)
			.addOnConnectionFailedListener(this)
			.build();

	apiClient.connect();
}

@DebugLog private void disconnectApiClient() {
	if (apiClient != null) {
		apiClient.disconnect();
		apiClient = null;
	}
}

@DebugLog private void stopApplication() {
	if (apiClient == null) return;

	if (applicationStarted) {
		Cast.CastApi.stopApplication(apiClient);
		applicationStarted = false;
	}
	EventBus.get().post(new CastSessionEvent(CastSessionEvent.Event.Stop));
}

@Override @DebugLog public void onApplicationDisconnected(int statusCode) {
	stopApplication();
}

@Override @DebugLog public void onVolumeChanged() {}

@DebugLog public void onStreamDurationChanged(final int duration) {
	this.mediaSessionDetails.setDuration(duration);
}

@Override @DebugLog public void onConnected(Bundle bundle) {
	try {

		final String sessionId = castPreferences.getSessionId();
		if (sessionId != null && !sessionId.isEmpty())
			executeJoinRequest(sessionId);
		else executeLaunchRequest();
	} catch (Exception e) {
		Log.e(TAG, "Failed to launch application", e);
	}
}

@DebugLog private void executeJoinRequest(final String sessionId) {
	Cast.CastApi.joinApplication(apiClient, APP_ID, sessionId)
	            .setResultCallback(new JoinRequestCallback(sessionId));
}

@DebugLog private void executeLaunchRequest() {
	Cast.CastApi.launchApplication(apiClient, APP_ID, false)
	            .setResultCallback(new LaunchRequestCallback());
}

@DebugLog private void onSessionConnected(final String sessionId, final String status) {
	applicationStarted = true;

	try {
		Cast.CastApi.setMessageReceivedCallbacks(apiClient, APP_NS, new IncomingMsgHandler());
	} catch (IOException e) {
		Log.e(TAG, "Exception while creating channel", e);
	}

	castPreferences.saveSessionId(sessionId);
	CastPlayer.get(context).init();
}

@DebugLog private void notifyListeners_onStartSession() {
	EventBus.get().post(new CastSessionEvent(CastSessionEvent.Event.Start));
}

@DebugLog private void notifyListeners_onStopCastSession() {
	EventBus.get().post(new CastSessionEvent(CastSessionEvent.Event.Stop));
}

@Override @DebugLog public void onConnectionSuspended(int i) {}

@Override @DebugLog public void onConnectionFailed(ConnectionResult result) { setCastDevice(null); }

private class IncomingMsgHandler implements Cast.MessageReceivedCallback {

	@DebugLog @Override public synchronized void onMessageReceived(
			CastDevice castDevice, String app, String statusJson) {

		if (statusJson == null) return;
		if (statusJson.isEmpty()) return;

		try {
			final CastStatusMessage message = new Gson()
					.fromJson(statusJson, CastStatusMessage.class);

			if (message == null) return;

			EventBus.get().post(message);

		} catch (JsonSyntaxException e) {
			e.printStackTrace();
		}
	}
}

private class MediaRouterCallbacks extends MediaRouter.Callback {
	@DebugLog @Override public void onRouteSelected(
			MediaRouter router, MediaRouter.RouteInfo route) {
		super.onRouteSelected(router, route);
		setCastDevice(CastDevice.getFromBundle(route.getExtras()));
	}

	@DebugLog @Override public void onRouteUnselected(
			MediaRouter router, MediaRouter.RouteInfo route) {
		super.onRouteUnselected(router, route);
		notifyListeners_onStopCastSession();
	}

	@DebugLog @Override public void onRouteAdded(
			MediaRouter router, MediaRouter.RouteInfo route) {
		super.onRouteAdded(router, route);
	}

	@DebugLog @Override public void onRouteRemoved(
			MediaRouter router, MediaRouter.RouteInfo route) {
		super.onRouteRemoved(router, route);
	}

	@DebugLog @Override public void onRouteChanged(
			MediaRouter router, MediaRouter.RouteInfo route) {
		super.onRouteChanged(router, route);
	}

	@DebugLog @Override public void onRouteVolumeChanged(
			MediaRouter router, MediaRouter.RouteInfo route) {
		super.onRouteVolumeChanged(router, route);
	}

	@DebugLog @Override public void onRoutePresentationDisplayChanged(
			MediaRouter router, MediaRouter.RouteInfo route) {
		super.onRoutePresentationDisplayChanged(router, route);
	}

	@DebugLog @Override public void onProviderAdded(
			MediaRouter router, MediaRouter.ProviderInfo provider) {
		super.onProviderAdded(router, provider);
	}

	@DebugLog @Override public void onProviderRemoved(
			MediaRouter router, MediaRouter.ProviderInfo provider) {
		super.onProviderRemoved(router, provider);
	}

	@DebugLog @Override public void onProviderChanged(
			MediaRouter router, MediaRouter.ProviderInfo provider) {
		super.onProviderChanged(router, provider);
	}
}

private class JoinRequestCallback implements ResultCallback<Cast.ApplicationConnectionResult> {
	private final String expectedSessionId;

	private JoinRequestCallback(String expectedSessionId) {
		this.expectedSessionId = expectedSessionId;
	}

	@Override public void onResult(Cast.ApplicationConnectionResult result) {
		if (result.getStatus().isSuccess()
				&& result.getSessionId().equals(this.expectedSessionId)) {
			onSessionConnected(result.getSessionId(), result.getApplicationStatus());
		} else executeLaunchRequest();
	}
}

private class LaunchRequestCallback implements ResultCallback<Cast.ApplicationConnectionResult> {
	@Override public void onResult(Cast.ApplicationConnectionResult result) {
		if (result.getWasLaunched())
			onSessionConnected(result.getSessionId(), result.getApplicationStatus());
	}
}

}
