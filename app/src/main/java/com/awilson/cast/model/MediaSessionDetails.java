package com.awilson.cast.model;


import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;

public class MediaSessionDetails implements Parcelable {

private String  playlist;
private int     playposition;
private int     duration;
private boolean liveStream;
private String  subtitle;
private String  title;
private String  itemId;

public MediaSessionDetails(final String playlist,
                           final int playposition,
                           final boolean liveStream,
                           final String title,
                           final String subtitle) {
	update(playlist, playposition, liveStream, title, subtitle);
}

public void update(final String playlist,
                   final int playposition,
                   final boolean liveStream,
                   final String title,
                   final String subtitle) {
	this.playlist = playlist;
	this.playposition = playposition;
	this.liveStream = liveStream;
	this.title = title;
	this.subtitle = subtitle;
}

public String getContentType() {
	return liveStream ? "application/x-mpegURL" : "video/mp4";
}

public int getMediaType() {
	return liveStream
	       ? MediaMetadata.MEDIA_TYPE_TV_SHOW
	       : MediaMetadata.MEDIA_TYPE_MOVIE;
}

public MediaMetadata getMetadata() {
	final MediaMetadata mediaMetadata = new MediaMetadata(getMediaType());

	if (title != null && !title.isEmpty())
		mediaMetadata.putString(MediaMetadata.KEY_TITLE, getTitle());

	if (subtitle != null && !subtitle.isEmpty())
		mediaMetadata.putString(MediaMetadata.KEY_SUBTITLE, getSubtitle());

	return mediaMetadata;
}

public MediaInfo getMediaInfo() {
	return new MediaInfo.Builder(playlist)
			.setContentType(getContentType())
			.setStreamType(getMediaType())
			.setMetadata(getMetadata())
			.build();
}

public void setDuration(int duration) {
	this.duration = duration;
}

public int getDuration() {
	return duration;
}

public String getItemId() {
	return itemId;
}

public void setItemId(String itemId) {
	this.itemId = itemId;
}

public void updatePlayPosition(final int playposition) {
	this.playposition = playposition;
}

public int getPlayPosition() {
	return playposition;
}

public boolean isLiveStream() {
	return liveStream;
}

public String getPlaylist() {
	return playlist;
}

public Uri getPlaylistUri() {
	return Uri.parse(getPlaylist());
}

public String getTitle() { return title;}

public String getSubtitle() {
	return subtitle;
}

@Override public String toString() {
	return "MediaSessionDetails{" +
			"playlist='" + playlist + '\'' +
			", playposition=" + playposition +
			", liveStream=" + liveStream +
			", title='" + title + '\'' +
			'}';
}


@Override public int describeContents() { return 0; }

@Override public void writeToParcel(Parcel dest, int flags) {
	dest.writeString(this.playlist);
	dest.writeInt(this.playposition);
	dest.writeInt(this.duration);
	dest.writeByte(liveStream ? (byte) 1 : (byte) 0);
	dest.writeString(this.subtitle);
	dest.writeString(this.title);
	dest.writeString(this.itemId);
}

private MediaSessionDetails(Parcel in) {
	this.playlist = in.readString();
	this.playposition = in.readInt();
	this.duration = in.readInt();
	this.liveStream = in.readByte() != 0;
	this.subtitle = in.readString();
	this.title = in.readString();
	this.itemId = in.readString();
}

public static final Creator<MediaSessionDetails> CREATOR = new Creator<MediaSessionDetails>() {
	public MediaSessionDetails createFromParcel(Parcel source) {
		return new MediaSessionDetails(source);
	}

	public MediaSessionDetails[] newArray(int size) {return new MediaSessionDetails[size];}
};
}
