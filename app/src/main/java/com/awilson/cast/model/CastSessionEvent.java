package com.awilson.cast.model;

public class CastSessionEvent {

public static enum Event {Start, Stop}

private final Event event;

public CastSessionEvent(final Event event) {
	this.event = event;
}

public Event getEvent() { return event;}

public boolean isStart() {return event == Event.Start;}

public boolean isStop() {return event == Event.Stop;}

}
