package com.awilson.cast.status;

import java.util.List;

public class Status {

int           mediaSessionId;
int           playbackRate;
String        playerState;
double        currentTime;
int           supportedMediaCommands;
Volume        volume;
List<Integer> activeTrackIds;
Media         media;

public int getMediaSessionId() { return mediaSessionId; }

public int getPlaybackRate() { return playbackRate; }

public String getPlayerState() { return playerState; }

public boolean isBuffering() {
	return "BUFFERING".equals(playerState);
}

public boolean isIdle() {
	return "IDLE".equals(playerState);
}

public boolean isPaused() {
	return "PAUSED".equals(playerState);
}

public boolean isPlaying() {
	return "PLAYING".equals(playerState);
}

public double getCurrentTime() { return currentTime; }

public int getSupportedMediaCommands() { return supportedMediaCommands; }

public Volume getVolume() { return volume; }

public List<Integer> getActiveTrackIds() { return activeTrackIds; }

public Media getMedia() { return media; }

@Override public String toString() {
	return "Status{" +
			"mediaSessionId=" + mediaSessionId +
			", playbackRate=" + playbackRate +
			", playerState='" + playerState + '\'' +
			", currentTime=" + currentTime +
			", supportedMediaCommands=" + supportedMediaCommands +
			", volume=" + volume +
			", activeTrackIds=" + activeTrackIds +
			", media=" + media +
			'}';
}

}
