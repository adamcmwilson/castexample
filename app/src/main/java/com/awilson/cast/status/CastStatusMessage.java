package com.awilson.cast.status;

import java.util.List;

public class CastStatusMessage {

String       type;
List<Status> status;
int          requestId;

public String getType() { return type;}

public List<Status> getStatus() { return status;}

public Status getLatestStatus() {
	if (status == null) return null;
	if (status.isEmpty()) return null;
	if (status.get(0) == null) return null;
	return status.get(0);
}

public boolean isLoadFailed() {
	return "LOAD_FAILED".equals(type);
}

public boolean isMediaStatus() {
	return "MEDIA_STATUS".equals(type);
}

public int getRequestId() { return requestId;}

@Override public String toString() {
	return "CastStatusMessage{" +
			"type='" + type + '\'' +
			", status=" + status +
			", requestId=" + requestId +
			'}';
}

}








