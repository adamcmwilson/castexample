package com.awilson.cast.status;

public class Volume {

private int     level;
private boolean muted;

public int getLevel() { return level; }

public boolean isMuted() { return muted; }

@Override public String toString() {
	return "Volume{" +
			"level=" + level +
			", muted=" + muted +
			'}';
}

}
