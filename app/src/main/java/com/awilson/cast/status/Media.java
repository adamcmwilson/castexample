package com.awilson.cast.status;

import java.util.List;

public class Media {

private String      contentId;
private String      streamType;
private String      contentType;
private Metadata    metadata;
private double      duration;
private List<Track> tracks;

public String getContentId() {
	return contentId;
}

public String getStreamType() {
	return streamType;
}

public String getContentType() {
	return contentType;
}

public Metadata getMetadata() {
	return metadata;
}

public double getDuration() {
	return duration;
}

public List<Track> getTracks() {
	return tracks;
}

@Override public String toString() {
	return "Media{" +
			"contentId='" + contentId + '\'' +
			", streamType='" + streamType + '\'' +
			", contentType='" + contentType + '\'' +
			", metadata=" + metadata +
			", duration=" + duration +
			", tracks=" + tracks +
			'}';
}

}
