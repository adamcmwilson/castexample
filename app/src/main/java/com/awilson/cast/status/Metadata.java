package com.awilson.cast.status;

public class Metadata {

private int metadataType;

public int getMetadataType() {
	return metadataType;
}

public void setMetadataType(int metadataType) {
	this.metadataType = metadataType;
}

@Override public String toString() {
	return "Metadata{" +
			"metadataType=" + metadataType +
			'}';
}

}
