package com.awilson.cast.status;

public class Track {

private int    trackId;
private String trackContentType;
private String type;
private Object name;
private Object language;

public int getTrackId() { return trackId; }

public String getTrackContentType() { return trackContentType; }

public String getType() { return type; }

public Object getName() { return name; }

public Object getLanguage() { return language; }

@Override public String toString() {
	return "Track{" +
			"trackId=" + trackId +
			", trackContentType='" + trackContentType + '\'' +
			", type='" + type + '\'' +
			", name=" + name +
			", language=" + language +
			'}';
}

}
