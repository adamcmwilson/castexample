package com.awilson.cast.util;

import java.util.concurrent.TimeUnit;

public class DateTimeFormatter {

public DateTimeFormatter() {

}

public String getTimeString(int milliseconds) {
	// Handle negatives by detecting < 0, then re-applying '-' after formatting
	boolean isNegative = milliseconds < 0;

	// Make milliseconds positive for format
	milliseconds = Math.abs(milliseconds);

	int hours = (int) TimeUnit.MILLISECONDS.toHours(milliseconds);
	int minutes = (int) TimeUnit.MILLISECONDS.toMinutes(milliseconds)
			- (int) TimeUnit.HOURS.toMinutes(hours);
	int seconds = (int) TimeUnit.MILLISECONDS.toSeconds(milliseconds)
			- (int) TimeUnit.HOURS.toSeconds(hours)
			- (int) TimeUnit.MINUTES.toSeconds(minutes);

	StringBuilder builder = new StringBuilder();

	// Hours ( only displayed if not 0 )
	if (hours > 0) {
		builder.append(String.valueOf(hours));
		builder.append(':');
	}

	// Minutes
	if (minutes < 10) builder.append('0');    // Leading 0
	builder.append(String.valueOf(minutes));
	builder.append(':');

	// Seconds
	if (seconds < 10) builder.append('0');    // Leading 0

	builder.append(String.valueOf(seconds));

	// Apply negative if necessary
	if (isNegative) builder.insert(0, '-');

	// All formatted!
	return builder.toString();
}

}
