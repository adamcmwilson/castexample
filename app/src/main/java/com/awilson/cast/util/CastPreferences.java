package com.awilson.cast.util;

import android.content.Context;
import android.content.SharedPreferences;

public class CastPreferences {

private final String SHARED_PREFS   = "Tablo.CastSharedPrefs";
private final String KEY_SESSION_ID = "SESSION_ID";
private final String KEY_ROUTE_ID   = "ROUTE_ID";

private SharedPreferences prefs;

public CastPreferences(Context context) {
	this.prefs = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
}

public void saveSessionId(final String sessionId) {
	this.prefs.edit().putString(KEY_SESSION_ID, sessionId).apply();
}

public void saveRouteId(final String routeId) {
	this.prefs.edit().putString(KEY_ROUTE_ID, routeId).apply();
}

public String getSessionId() {
	return this.prefs.getString(KEY_SESSION_ID, "");
}

public String getRouteId() {return this.prefs.getString(KEY_ROUTE_ID, "");}

public void clearSession() {
	this.prefs.edit().remove(KEY_SESSION_ID).apply();
}

public void clearRoute() {
	this.prefs.edit().remove(KEY_ROUTE_ID).apply();
}


}
