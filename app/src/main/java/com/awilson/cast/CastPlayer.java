package com.awilson.cast;

import android.content.Context;
import android.util.Log;

import com.awilson.cast.eventbus.EventBus;
import com.awilson.cast.model.CastSessionEvent;
import com.awilson.cast.model.MediaSessionDetails;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.cast.RemoteMediaPlayer;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import hugo.weaving.DebugLog;
import rx.Observable;
import rx.Observer;
import rx.functions.Func1;

public class CastPlayer {

private final static String TAG = "CastPlayer";
private static CastPlayer        instance;
private static RemoteMediaPlayer player;
private static boolean           isPlaying;
private        Context           context;

private CastPlayer(Context context) {
	this.context = context;
}

public static CastPlayer get(Context context) {
	if (instance == null) instance = new CastPlayer(context);
	return instance;
}

public void init() {
	CastSession.get(context).mediaSessionDetails()
	           .subscribe(new MediaSessionDetailsObserver());
}

@DebugLog private void preparePlayer(final MediaSessionDetails mediaSessionDetails) {

	player = new RemoteMediaPlayer();
	player.setOnStatusUpdatedListener(new StatusListener());
	player.setOnMetadataUpdatedListener(new MetaListener());

	try {
		Cast.CastApi.setMessageReceivedCallbacks(
				getApiClient(), player.getNamespace(), player);
	} catch (IOException e) {
		Log.e(TAG, "Exception while creating media channel", e);
	}

	player.requestStatus(getApiClient()).setResultCallback(
			new InitialStatusRequestCallback(mediaSessionDetails));
}

@DebugLog private void initComplete(final MediaSessionDetails mediaSessionDetails) {
	try {
		final String currentContentId = player.getMediaInfo().getContentId();
		final String newContentId = mediaSessionDetails.getMediaInfo().getContentId();

		if (currentContentId.equals(newContentId)) {
			Log.i(TAG, "MATCH");
			player.requestStatus(getApiClient());
			return;
		}

	} catch (Exception e) {
		e.printStackTrace();
		Log.w(TAG, e.getMessage());
		loadMedia(mediaSessionDetails);
	}

	loadMedia(mediaSessionDetails);
}

private GoogleApiClient getApiClient() {return CastSession.get(context).getApiClient();}

private void loadMedia(MediaSessionDetails mediaSessionDetails) {
	try {
		player.load(getApiClient(),
		            mediaSessionDetails.getMediaInfo(),
		            true,
		            mediaSessionDetails.getPlayPosition())
		      .setResultCallback(new PlayerLoadCallback());
	} catch (Exception e) {
		e.printStackTrace();
		EventBus.get().post(new CastSessionEvent(CastSessionEvent.Event.Stop));
	}
}

@DebugLog public void togglePlayback() {
	try {
		if (isPlaying)
			player.pause(getApiClient()).setResultCallback(new PlaybackCallback());
		else player.play(getApiClient()).setResultCallback(new PlaybackCallback());
	} catch (Exception e) {
		Log.wtf(TAG, e);
		e.printStackTrace();
		EventBus.get().post(new CastSessionEvent(CastSessionEvent.Event.Stop));
	}
}

@DebugLog public void seekBy(final int seekAmt) {
	seekTo((int) (player.getApproximateStreamPosition() + seekAmt));
}

@DebugLog public void seekTo(final int position) {
	try {
		player.seek(getApiClient(), position).setResultCallback(new SeekResult());
	} catch (Exception e) {
		e.printStackTrace();
		EventBus.get().post(new CastSessionEvent(CastSessionEvent.Event.Stop));
	}
}

private Observable<Long> observablePlaybackPosition;

public Observable<Long> observePlaybackPosition() {
	if (observablePlaybackPosition == null) {
		observablePlaybackPosition =
				Observable.interval(1, TimeUnit.SECONDS)
				          .flatMap(new Func1<Long, Observable<Long>>() {
					          @Override public Observable<Long> call(Long aLong) {
						          return Observable.just(player.getApproximateStreamPosition());
					          }
				          });
	}
	return observablePlaybackPosition;
}

private class SeekResult implements ResultCallback<RemoteMediaPlayer.MediaChannelResult> {
	@DebugLog @Override
	public void onResult(RemoteMediaPlayer.MediaChannelResult result) {
		if (result != null) Log.i(TAG, "SEEK RESULT: " + result.toString());
	}
}

@DebugLog public void stop() {
	player.stop(getApiClient()).setResultCallback(
			new ResultCallback<RemoteMediaPlayer.MediaChannelResult>() {
				@DebugLog @Override
				public void onResult(RemoteMediaPlayer.MediaChannelResult result) {
					if (result != null) Log.i(TAG, "STOP RESULT: " + result.toString());
				}
			});
}

static class StatusListener implements RemoteMediaPlayer.OnStatusUpdatedListener {
	@DebugLog @Override public void onStatusUpdated() {
		if (player.getMediaStatus() == null) return; // cannot post null...
		EventBus.get().post(player.getMediaStatus());

		switch (player.getMediaStatus().getPlayerState()) {
			case MediaStatus.PLAYER_STATE_PLAYING:
			case MediaStatus.PLAYER_STATE_BUFFERING:
				isPlaying = true;
				break;
			case MediaStatus.PLAYER_STATE_PAUSED:
			case MediaStatus.PLAYER_STATE_IDLE:
			case MediaStatus.PLAYER_STATE_UNKNOWN:
				isPlaying = false;
				break;
		}
	}
}

static class MetaListener implements RemoteMediaPlayer.OnMetadataUpdatedListener {
	@DebugLog @Override public void onMetadataUpdated() {
//		MediaInfo mediaInfo = player.getMediaInfo();
//		MediaMetadata metadata = mediaInfo.getMetadata();
	}
}

private class PlaybackCallback implements ResultCallback<RemoteMediaPlayer.MediaChannelResult> {

	@DebugLog @Override
	public void onResult(RemoteMediaPlayer.MediaChannelResult result) { }
}

private class PlayerLoadCallback implements ResultCallback<RemoteMediaPlayer.MediaChannelResult> {
	@DebugLog @Override public void onResult(RemoteMediaPlayer.MediaChannelResult result) {
		try {
//			Log.w(TAG, "MEDIA CHANNEL RESULT:  " + result.toString());
//			Log.w(TAG, "CUSTOM DATA:  " + result.getCustomData().toString());
		} catch (Exception e) {
			// ignore
			e.printStackTrace();
		}
	}
}

private class InitialStatusRequestCallback
		implements ResultCallback<RemoteMediaPlayer.MediaChannelResult> {

	private final MediaSessionDetails mediaSessionDetails;

	private InitialStatusRequestCallback(MediaSessionDetails mediaSessionDetails) {
		this.mediaSessionDetails = mediaSessionDetails;
	}

	@Override public void onResult(RemoteMediaPlayer.MediaChannelResult result) {
		initComplete(mediaSessionDetails);
	}
}

private class MediaSessionDetailsObserver implements Observer<MediaSessionDetails> {
	@DebugLog @Override public void onNext(MediaSessionDetails mediaSessionDetails) {
		preparePlayer(mediaSessionDetails);
	}

	@DebugLog @Override public void onError(Throwable e) { }

	@DebugLog @Override public void onCompleted() { }
}

}
